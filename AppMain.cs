using System;
using System.Web;
using System.IO;
using System.Text;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Reflection;
using System.Diagnostics;
using System.Collections;
using System.Collections.Specialized;
using System.Runtime.InteropServices;
using System.Runtime.CompilerServices;
using System.Windows.Forms;
using System.Security.Principal;
using Microsoft.Win32;
using System.Collections.Generic;
using System.Globalization;

namespace applaunchProtocolHandler
{
	public static class AppMain
	{
		#region Argument Serialization
		private const string ARGSER = "argser:";
		/// <summary>
		/// Serialize command line arguments into a "safe" format that doesn't need
		/// shell escaping.  This is because the shell escaping rules for DOS/Windows
		/// are fairly insane and don't behave consistently from command to command.
		/// Instead, serialize data to our own format, which we will recognize on
		/// startup and deserialize transparently.
		/// </summary>
		private static string SerializeArgs(this string[] Args)
		{
			List<byte> Bytes = new List<byte>();
			Action<ushort> AddShort = S => Bytes.AddRange(
				BitConverter.IsLittleEndian ? BitConverter.GetBytes(S).Reverse() : BitConverter.GetBytes(S));
			if ( Args.Length > ushort.MaxValue )
				throw new Exception("Too many arguments.");
			AddShort((ushort)Args.Length);
			foreach ( string Arg in Args )
			{
				byte[] ArgBytes = Encoding.UTF8.GetBytes(Arg);
				if ( ArgBytes.Length > ushort.MaxValue )
					throw new Exception("Arg too long.");
				AddShort((ushort)ArgBytes.Length);
				Bytes.AddRange(ArgBytes);
			}
			return ARGSER + string.Join(string.Empty, Bytes.Select(B => B.ToString("X2")));
		}
		/// <summary>
		/// Transparently deserialize arguments that were serialized into
		/// our own internal "safe" format.
		/// </summary>
		private static string[] DeserializeArgs(this string[] MaybeSer)
		{
			if ( MaybeSer.Length != 1 )
				return MaybeSer;
			string Ser = MaybeSer[0];
			if ( !Ser.StartsWith(ARGSER, StringComparison.CurrentCultureIgnoreCase) )
				return MaybeSer;
			if ( (Ser.Length - ARGSER.Length) % 2 != 0 )
				throw new Exception("Serialized data must be even length.");
			int Pos = ARGSER.Length;
			string[] Args = new string[int.Parse(Ser.Substring(Pos, 4), NumberStyles.HexNumber)];
			Pos += 4;
			for ( int Idx = 0; Idx < Args.Length; Idx++ )
			{
				int BLen = int.Parse(Ser.Substring(Pos, 4), NumberStyles.HexNumber);
				Pos += 4;
				byte[] ArgBytes = new byte[BLen];
				for ( int P = 0; P < BLen; P++ )
					ArgBytes[P] = byte.Parse(Ser.Substring(Pos + P * 2, 2), NumberStyles.HexNumber);
				Args[Idx] = Encoding.UTF8.GetString(ArgBytes);
				Pos += BLen * 2;
			}
			return Args;
		}
		#endregion

		#region Exception Handling
		private static void HandleException(Exception ex)
		{
			List<string> Traces = new List<string>();
			for ( Exception e = ex; e != null; e = e.InnerException )
				Traces.Add(e.ToString());
			MessageBox.Show(string.Join(Environment.NewLine + Environment.NewLine, Traces),
				Application.ProductName + " v" + Application.ProductVersion + " Error",
				MessageBoxButtons.OK, MessageBoxIcon.Error);
		}
		private static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
		{
			if ( (e.ExceptionObject != null) && (e.ExceptionObject is Exception) )
				HandleException((Exception)e.ExceptionObject);
		}
		private static void Application_ThreadException(object sender, ThreadExceptionEventArgs e)
		{
			if ( e.Exception != null )
				HandleException(e.Exception);
		}
		#endregion

		#region Application Main Entry Point
		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main(string[] Args)
		{
			// Register internal exception handling, as the standard OS-level one
			// doesn't return any debugging info.
			AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(CurrentDomain_UnhandledException);
			Application.ThreadException += new ThreadExceptionEventHandler(Application_ThreadException);

			// Automatically restore any serialized args.
			Args = Args.DeserializeArgs();

			// Automatically install app locally, if it's not running
			// from an already-installed location.
			AutoSelfInstall(Args);

			// Now, look at the URL passed in by user.  ClearCache() will clear the
			// app cache, while no URL will simply display a notification that the
			// protocol handler has been installed.  Other URL's will launch the
			// actual app pointed to in the URL.
			LaunchOptions Options = LaunchOptions.FromCommandLine(Args);
			if ( Options.LaunchURL != string.Empty )
			{
				if ( Settings.BlackList.Contains(Options.ToString()) )
					return;
				else if ( Settings.WhiteList.Contains(Options.ToString()) )
					Application.Run(new FormLaunch(Options));
				else
					Application.Run(new FormAsk(Options));
			}
			else
				Application.Run(new FormConfig());
		}
		#endregion

		#region SetValueAuto
		/// <summary>
		/// Set a registry key's value, only if it hasn't already been set to the same value.
		/// </summary>
		public static void SetValueAuto(this RegistryKey RootKey, string SubKey, string Name, string Value)
		{
			if ( RootKey == null )
				return;
			RegistryKey Key = RootKey.OpenSubKey(SubKey);
			if ( Key != null )
			{
				object OldVal = Key.GetValue(Name);
				if ( object.Equals(OldVal, null) && object.Equals(Value, null) )
					return;
				if ( !object.Equals(OldVal, null) && OldVal.ToString().Equals(Value, StringComparison.CurrentCultureIgnoreCase) )
					return;
			}
			Key = RootKey.CreateSubKey(SubKey);
			Key.SetValue(Name, Value);
		}
		#endregion

		#region Public Static Method AutoSelfInstall
		/// <summary>
		/// Automatically install this app locally, if it's
		/// being run from any location other than its installation
		/// destination.  Register it to handle the applaunch:
		/// protocol, and clean up any old version.
		/// </summary>
		public static void AutoSelfInstall(string[] Args)
		{
			// Calculate the installation path names on the
			// local system.
			string CleanAppName = Regex.Replace(
				Application.ProductName, "[^A-Za-z0-9]", "");
			DirectoryInfo InstPath = new DirectoryInfo(Path.Combine(Environment.GetFolderPath(
				Environment.SpecialFolder.ApplicationData),
				CleanAppName + "-" + Application.ProductVersion));
			FileInfo InstExe = new FileInfo(Path.Combine(InstPath.FullName,
				Path.GetFileName(Application.ExecutablePath)));

			// Check if the application is running from its
			// correct installation "home" dir.  If not, it
			// needs to be installed.
			if ( !InstExe.FullName.Equals(Application.ExecutablePath, StringComparison.CurrentCultureIgnoreCase) )
			{
				// Create the installation path and copy the app
				// to it.  Remove any read-only flags from the file,
				// so it can be automatically deleted later if a
				// new version needs to be installed.
				if ( !InstExe.Exists )
				{
					if ( !InstPath.Exists )
						InstPath.Create();
					File.Copy(Application.ExecutablePath, InstExe.FullName, true);
					InstPath.Attributes = FileAttributes.Normal;
				}

				// Re-launch this app from its new home, and signal
				// the current application to shut down.
				Process.Start(InstExe.FullName, Args.SerializeArgs());
				Application.Exit();
				Process.GetCurrentProcess().Kill();
				return;
			}

			// Register the application as the handler for the applanch: protocol.
			// This way we can use URL's like applaunch://path/to/app to run this app.
			const string PROTOCOL_SCHEME = "applaunch";
			Microsoft.Win32.Registry.CurrentUser.SetValueAuto(@"SOFTWARE\Classes\" + PROTOCOL_SCHEME,
				null, "URL:" + Application.ProductName);
			Microsoft.Win32.Registry.CurrentUser.SetValueAuto(@"SOFTWARE\Classes\" + PROTOCOL_SCHEME,
				"URL Protocol", "");
			Microsoft.Win32.Registry.CurrentUser.SetValueAuto(@"SOFTWARE\Classes\" + PROTOCOL_SCHEME + @"\DefaultIcon",
				null, "\"" + Application.ExecutablePath + "\"");
			Microsoft.Win32.Registry.CurrentUser.SetValueAuto(@"SOFTWARE\Classes\" + PROTOCOL_SCHEME + @"\shell\open\command",
				null, "\"" + Application.ExecutablePath + "\" \"%1\"");

			// Search for any old copies of the AppLaunch proto handler
			// and remove them from the system.
			foreach ( DirectoryInfo DI in InstPath.Parent.EnumerateDirectories(CleanAppName + "-*") )
				if ( !DI.Name.Equals(InstPath.Name, StringComparison.CurrentCultureIgnoreCase) )
					try { DI.Delete(true); }
					catch { }
		}
		#endregion

		#region Public Static Method DisplayError
		/// <summary>
		/// Displays a standard critical error dialog.
		/// </summary>
		public static void DisplayError(string Message)
		{
			MessageBox.Show(Message.Replace(Environment.NewLine, "\n")
				.Replace("\n", Environment.NewLine), Application.ProductName + " Error",
				MessageBoxButtons.OK, MessageBoxIcon.Error);
		}
		#endregion

		#region Public Static Method DisplayNotice
		/// <summary>
		/// Displays a standard informational notice dialog.
		/// </summary>
		public static void DisplayNotice(string Message)
		{
			MessageBox.Show(Message.Replace(Environment.NewLine, "\n")
				.Replace("\n", Environment.NewLine), Application.ProductName + " Notice",
				MessageBoxButtons.OK, MessageBoxIcon.Information);
		}
		#endregion
	}
}


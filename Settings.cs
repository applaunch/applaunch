﻿using System.Collections.Specialized;
using Microsoft.Win32;
using System.Windows.Forms;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace applaunchProtocolHandler
{
	public class BlackWhiteList : ICollection<string>
	{
		private RegistryKey MyKey;

		public BlackWhiteList(RegistryKey key)
		{
			MyKey = key;
		}

		private string GetKey(string value)
		{
			return string.Join(string.Empty, new MD5CryptoServiceProvider()
				.ComputeHash(Encoding.UTF8.GetBytes(value.ToLower()))
				.Select(B => B.ToString("x2")));
		}

		public void Clear()
		{
			foreach ( string ID in MyKey.GetValueNames() )
				MyKey.DeleteValue(ID);
		}

		public bool Contains(string item)
		{
			string ID = GetKey(item);
			object O = MyKey.GetValue(ID);
			return O != null;
		}

		public void CopyTo(string[] array, int arrayIndex)
		{
			foreach ( string ID in MyKey.GetValueNames() )
			{
				object O = MyKey.GetValue(ID);
				if ( O != null )
				{
					array[arrayIndex] = O.ToString();
					if ( ++arrayIndex >= array.Length )
						break;
				}
			}
		}

		public int Count
		{
			get { return MyKey.GetValueNames().Length; }
		}

		public bool IsReadOnly
		{
			get { return false; }
		}

		bool ICollection<string>.Remove(string item)
		{
			string ID = GetKey(item);
			try { MyKey.DeleteValue(ID, true); return true; }
			catch { return false; }
		}

		public IEnumerator<string> GetEnumerator()
		{
			return MyKey.GetValueNames().Select(X => (MyKey.GetValue(X) ?? string.Empty).ToString()).GetEnumerator();
		}

		System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
		{
			return GetEnumerator();
		}

		public void Add(string item)
		{
			string ID = GetKey(item);
			MyKey.SetValue(ID, item);
		}
	}

	public static class Settings
	{
		private static RegistryKey MyKey
		{
			get
			{
				return Registry.CurrentUser.CreateSubKey(@"Software\AppLaunch");
			}
		}

		private static ICollection<string> _BlackList = null;
		public static ICollection<string> BlackList
		{
			get
			{
				if(_BlackList == null)
					_BlackList = new BlackWhiteList(MyKey.CreateSubKey("Blocked"));
				return _BlackList;
			}
		}

		private static ICollection<string> _WhiteList = null;
		public static ICollection<string> WhiteList
		{
			get
			{
				if ( _WhiteList == null )
					_WhiteList = new BlackWhiteList(MyKey.CreateSubKey("Allowed"));
				return _WhiteList;
			}
		}
	}
}

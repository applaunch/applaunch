﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace applaunchProtocolHandler
{
	public partial class FormConfig : Form
	{
		private const string ALLOW = "Allowed";
		private const string BLOCK = "Blocked";

		public FormConfig()
		{
			InitializeComponent();
		}

		private void ReloadRules()
		{
			if ( Settings.BlackList.Any() || Settings.WhiteList.Any() )
			{
				listViewRules.BeginUpdate();
				listViewRules.Items.Clear();
				listViewRules.Items.AddRange(Settings.BlackList
					.Select(X => new ListViewItem(new string[] { BLOCK, X }))
					.Concat(Settings.WhiteList.Select(X => new ListViewItem(new string[] { ALLOW, X })))
					.OrderBy(X => X.SubItems[1].Text, StringComparer.CurrentCultureIgnoreCase)
					.ToArray());
				listViewRules.AutoResizeColumns(ColumnHeaderAutoResizeStyle.ColumnContent);
				listViewRules.EndUpdate();
				listViewRules.Visible = true;
				labelNoRules.Visible = false;
			}
			else
			{
				listViewRules.Items.Clear();
				listViewRules.Visible = false;
				labelNoRules.Visible = true;
			}
		}
		private void FormConfig_Load(object sender, EventArgs e)
		{
			Text = Application.ProductName + " v" + Application.ProductVersion;
			ReloadRules();
		}

		private void buttonClose_Click(object sender, EventArgs e)
		{
			Close();
		}

		private void buttonRemoveRule_Click(object sender, EventArgs e)
		{
			foreach(ListViewItem Item in listViewRules.SelectedItems)
				switch ( Item.SubItems[0].Text )
				{
					case ALLOW:
						Settings.WhiteList.Remove(Item.SubItems[1].Text);
						break;
					case BLOCK:
						Settings.BlackList.Remove(Item.SubItems[1].Text);
						break;
				}
			ReloadRules();
		}
	}
}

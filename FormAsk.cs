﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace applaunchProtocolHandler
{
    public partial class FormAsk : Form
    {
        protected LaunchOptions Options = null;

        public FormAsk(LaunchOptions NewOptions)
        {
            Options = NewOptions;
            InitializeComponent();
        }

        private void FormAsk_Load(object sender, EventArgs e)
        {
            this.Text = "Launch Query - " + Application.ProductName;
            this.labelURL.Text = Options.ToString();
        }

        private void buttonBlockOnce_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void buttonAllowOnce_Click(object sender, EventArgs e)
        {
            this.Hide();
            new FormLaunch(Options).ShowDialog();
            this.Close();
        }

        private void buttonAllowAlways_Click(object sender, EventArgs e)
        {
			Settings.WhiteList.Add(Options.ToString());
            buttonAllowOnce.PerformClick();
        }

        private void buttonBlockAlways_Click(object sender, EventArgs e)
        {
			Settings.BlackList.Add(Options.ToString());
            buttonBlockOnce.PerformClick();
        }
    }
}
